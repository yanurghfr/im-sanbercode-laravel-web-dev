<?php
require("animal.php");
require("frog.php");
require("ape.php");

$a = new Animal("Shaun");
echo "Name : $a->name <br>";
echo "Legs : $a->legs <br>";
echo "cold blooded : $a->cold_blooded <br><br>";

$b = new Frog("buduk");
echo "Name : $b->name <br>";
echo "Legs : $b->legs <br>";
echo "cold blooded : $b->cold_blooded <br>";
$b->jump();

$c = new Ape("kera sakti");
echo "Name : $c->name <br>";
echo "Legs : $c->legs <br>";
echo "cold blooded : $c->cold_blooded <br>";
$c->yell();
