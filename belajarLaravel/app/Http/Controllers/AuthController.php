<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('page.register');
    }

    public function daftar(Request $request)
    {
        $fname = $request->input('fname');
        $lname = $request->input('lname');

        return view('page.welcome', compact('fname', 'lname'));
    }
}
