<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TablesController extends Controller
{
    public function table()
    {
        return view('page.table');
    }

    public function dataTable()
    {
        return view('page.data-table');
    }
}
