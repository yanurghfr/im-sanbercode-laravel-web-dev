<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TablesController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::get('/register', [AuthController::class, 'index']);

Route::post('/welcome', [AuthController::class, 'daftar']);

Route::get('/table', [TablesController::class, 'table']);

Route::get('/data-table', [TablesController::class, 'dataTable']);

Route::get('/cast', [CastController::class, 'index']);

Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);

Route::get('/cast/{id}', [CastController::class, 'show']);

Route::get('/cast/{id}/edit', [CastController::class, 'edit']);

Route::put('/cast/{id}', [CastController::class, 'update']);

Route::delete('/cast/{id}', [CastController::class, 'destroy']);
