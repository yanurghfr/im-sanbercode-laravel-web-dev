@extends('layouts.master')

@section('title')
Sing Up
@endsection

@section('content')

<body>
    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label for="fname">First name:</label><br><br>
        <input type="text" name="fname" id="fname"><br><br>
        <label for="lname">Last name:</label><br><br>
        <input type="text" name="lname" id="lname"><br><br>
        <label for="gender">Gender:</label><br><br>
        <input type="radio" name="gender" id="gender" value="1">Male<br>
        <input type="radio" name="gender" id="gender" value="2">Female<br>
        <input type="radio" name="gender" id="gender" value="3">Other<br><br>
        <label for="nation">Nationality:</label><br><br>
        <Select name="nation" id="nation">
            <option value="Indonesia">Indonesian</option>
            <option value="Singapore">Singaporian</option>
            <option value="Malaysia">Malaysian</option>
            <option value="Australia">Australian</option>
        </Select><br><br>
        <label for="langg">Language Spoken:</label><br><br>
        <input type="checkbox" name="langg[]" id="langg" value="1">Bahasa Indonesia<br>
        <input type="checkbox" name="langg[]" id="langg" value="2">English<br>
        <input type="checkbox" name="langg[]" id="langg" value="3">Other<br><br>
        <label for="bio">Bio:</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="signup">
    </form>
    @endsection