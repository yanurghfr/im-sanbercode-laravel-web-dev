@extends('layouts.master')

@section('title')
Detail Cast
@endsection

@section('content')
<h1>{{$cast->name}}, {{$cast->age}} tahun </h1>
<p>{{$cast->bio}}</p>
@endsection